package com.example.fernanda.app_geolocalizacion.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fernanda.app_geolocalizacion.Class.PlaceInfo;
import com.example.fernanda.app_geolocalizacion.R;

import java.util.List;

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.ViewHolder>{

    private List<PlaceInfo> placeInfos;

    public PlacesAdapter(List<PlaceInfo> placeInfos){ this.placeInfos = placeInfos; }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview_places,viewGroup,false);
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesAdapter.ViewHolder viewHolder, int i) {
        viewHolder.name.setText(placeInfos.get(i).getName());
        viewHolder.address.setText(placeInfos.get(i).getAddress());
        viewHolder.phone.setText(placeInfos.get(i).getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return placeInfos.size();
    }

    public void setItems(List<PlaceInfo> placeInfos){ this.placeInfos = placeInfos; }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        TextView address;
        TextView phone;

        public ViewHolder(@NonNull final View itemView){

            super(itemView);
            name = itemView.findViewById(R.id.textViewPlace);
            address = itemView.findViewById(R.id.textViewAddress);
            phone = itemView.findViewById(R.id.textViewPhone);


        }
    }







}

