package com.example.fernanda.app_geolocalizacion.Fragments;

import android.app.Activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import static android.app.Activity.RESULT_CANCELED;
import static android.support.constraint.Constraints.TAG;
import static com.example.fernanda.app_geolocalizacion.R.id.place_autocomplete_fragment;

import android.Manifest;

import com.example.fernanda.app_geolocalizacion.Adapters.PlaceAutocompleteAdapter;
import com.example.fernanda.app_geolocalizacion.Class.PlaceInfo;
import com.example.fernanda.app_geolocalizacion.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapFragment extends Fragment implements OnConnectionFailedListener, GoogleMap.OnMyLocationClickListener,
GoogleMap.OnMyLocationButtonClickListener,OnMapReadyCallback, AdapterView.OnItemClickListener {

    private static View rootView;
    private GoogleMap gMap;
    private MapView mapView;
    private final int MY_LOCATION_REQUEST_CODE = 1;
    private GoogleApiClient mGoogleApiClient;
    static PlaceAutocompleteFragment autocompleteFragment;
    AutocompleteFilter typeFilter;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2;
    private PlaceInfo mPlace;
    private boolean ubicacion;
    private PlaceAutocompleteAdapter mPlaceAutoCompleteAdapater;

    public MapFragment() {  }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(rootView != null){
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if(parent != null)
                parent.removeView(rootView);
        }try {
            rootView = inflater.inflate(R.layout.fragment_map, container, false);
            mGoogleApiClient = new GoogleApiClient
                    .Builder(getActivity())
                    .enableAutoManage(getActivity(),0,this)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
            autocompleteFragment = (PlaceAutocompleteFragment)
                    getActivity().getFragmentManager().findFragmentById(place_autocomplete_fragment);
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    String placeName = place.getLatLng().toString();
                    String result = placeName.replaceAll("[|?**<\":>+\\[\\]/'latlng():]","").trim();
                    String[] latlon = result.split(",");
                    LatLng reforma = new LatLng(Double.parseDouble(latlon[0]),Double.parseDouble(latlon[1]));
                    Toast.makeText(getActivity(),""+placeName,Toast.LENGTH_SHORT).show();
                    gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(reforma,7));
                    CameraPosition camera = new CameraPosition.Builder()
                            .target(reforma)
                            .zoom(14)
                            .bearing(0)
                            .tilt(90).build();
                    gMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));
                    gMap.addMarker(new MarkerOptions()
                    .position(reforma)
                    .title(""));

                }
                @Override
                public void onError(Status status) {       }
            });
        }catch (InflateException e){

        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = rootView.findViewById(R.id.map);
        typeFilter = new AutocompleteFilter.Builder()
                .setCountry("MEX")
                .build();
        autocompleteFragment.setFilter(typeFilter);
        try{
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());
            //getActivity().startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        }catch (GooglePlayServicesNotAvailableException e){
            e.printStackTrace();
        }catch (GooglePlayServicesRepairableException e){
            e.printStackTrace();
        }
        setRetainInstance(true);

        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.animateCamera(CameraUpdateFactory.zoomBy(8));
        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng reforma = new LatLng(19.4785099,-99.2396317);
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(reforma,8));
        CameraPosition camera = new CameraPosition.Builder()
                .target(reforma)
                .zoom(7)
                .bearing(0)
                .tilt(90)
                .build();
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));
        ubicacion = true;
        marshmallowGPSPermissionCheck();
        gMap.setOnMyLocationButtonClickListener(this);
        gMap.setOnMyLocationClickListener(this);

    }

    private void marshmallowGPSPermissionCheck(){
        int permission = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if(permission == PackageManager.PERMISSION_GRANTED){
            gMap.setMyLocationEnabled(true);
        }else {
            ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_LOCATION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode){
            case MY_LOCATION_REQUEST_CODE:{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                //lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,2000,10, locationListener);
                }else{  //permission denied, boo! Disable the
                    //functionality that depends on this permission
                }
                break;
            }
        }
    }

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {       }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {     }

        @Override
        public void onProviderEnabled(String s) {     }

        @Override
        public void onProviderDisabled(String s) {     }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE){
            Place place = PlaceAutocomplete.getPlace(getActivity(),data);
            Log.i(TAG,"Place: " + place.getName());
        }else if(resultCode == PlaceAutocomplete.RESULT_ERROR){
            Status status = PlaceAutocomplete.getStatus(getActivity(),data);
            Log.i(TAG, status.getStatusMessage());
        }else if(resultCode == RESULT_CANCELED){

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        final AutocompletePrediction item = mPlaceAutoCompleteAdapater.getItem(i);
        final String placeId = item.getPlaceId();

        Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(@NonNull PlaceBuffer places) {
                        if(places.getStatus().isSuccess() && places.getCount() > 0){
                            final Place myPlace = places.get(0);
                            mPlace = new PlaceInfo();
                            mPlace.setName(myPlace.getName().toString());
                            mPlace.setAddress(myPlace.getName().toString());
                            mPlace.setAttributions(myPlace.getAttributions().toString());
                            mPlace.setId(myPlace.getId().toString());
                            mPlace.setLatlng(myPlace.getLatLng());
                            mPlace.setRating(myPlace.getRating());
                            mPlace.setPhoneNumber(myPlace.getPhoneNumber().toString());
                            mPlace.setWebsiteUri(myPlace.getWebsiteUri());
                            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPlace.getLatLng(),7));
                        }else {
                            Log.e(TAG,"No se encontró el lugar");
                        }
                        places.release();
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {    }

    @Override
    public boolean onMyLocationButtonClick() {  return false;   }

    @Override
    public void onMyLocationClick(@NonNull Location location) {    }

    @Override
    public void onPause() {    super.onPause();   }

    @Override
    public void onResume() {   super.onResume();  }

    @Override
    public void onStop() {   super.onStop();
        if(mGoogleApiClient != null && mGoogleApiClient.isConnected()){
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(autocompleteFragment != null && getActivity() != null && !getActivity().isFinishing()){
            getActivity().getFragmentManager().beginTransaction().remove(autocompleteFragment).commit();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment f = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.place_autocomplete_fragment);
        if(f != null){
            getFragmentManager().beginTransaction().remove(f).commit();
        }
    }
}
