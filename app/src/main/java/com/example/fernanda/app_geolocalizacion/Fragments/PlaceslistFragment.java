package com.example.fernanda.app_geolocalizacion.Fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.fernanda.app_geolocalizacion.Adapters.PlacesAdapter;
import com.example.fernanda.app_geolocalizacion.Class.PlaceInfo;
import com.example.fernanda.app_geolocalizacion.R;

import java.util.ArrayList;
import java.util.List;

public class PlaceslistFragment extends Fragment {

    private View view;
    LinearLayoutManager linearLayoutManager;
    RecyclerView rvplace;
    PlacesAdapter adapter;

    private List<PlaceInfo> placeInfoList = new ArrayList<>();


    public PlaceslistFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_placeslist, container, false);
        rvplace = view.findViewById(R.id.rv);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvplace.setLayoutManager(linearLayoutManager);
        return view;
    }

    public void LoadAdapter(List<PlaceInfo> place){
        if (adapter == null){
            adapter = new PlacesAdapter(place);
            rvplace.setAdapter(adapter);
        }else{
            adapter.setItems(place);
            adapter.notifyDataSetChanged();
        }
    }

}
