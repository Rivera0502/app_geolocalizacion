package com.example.fernanda.app_geolocalizacion.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.fernanda.app_geolocalizacion.Class.MapPojo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ResultCallback;
import com.example.fernanda.app_geolocalizacion.Adapters.PlaceAutocompleteAdapter;
import com.example.fernanda.app_geolocalizacion.Class.PlaceInfo;
import com.example.fernanda.app_geolocalizacion.R;
import static android.app.Activity.RESULT_CANCELED;

import static android.support.constraint.Constraints.TAG;
import static com.example.fernanda.app_geolocalizacion.R.id.place_autocomplete_fragment_gas;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class GasolinaFragment extends Fragment implements GoogleMap.OnMyLocationClickListener,
GoogleMap.OnMyLocationButtonClickListener, OnMapReadyCallback, AdapterView.OnItemClickListener, GoogleApiClient.OnConnectionFailedListener {

    private MapView mapViewG;
    private GoogleMap gMap;
    private static View rootview;
    private boolean ubicacion;
    private GoogleApiClient googleApiClient;
    static PlaceAutocompleteFragment autocompleteFragment;
    AutocompleteFilter typefilter;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 2;
    private final int MY_LOCATION_REQUEST_CODE = 1;
    private PlaceInfo placeInfo;
    private PlaceAutocompleteAdapter placeAutocompleteAdapter;
    private FusedLocationProviderClient mFusedLocationClient;
    DatabaseReference databaseReference;
    private FirebaseAuth mAuth;
    private ArrayList<Marker> tempRealtimeMarkers = new ArrayList<>();
    private ArrayList<Marker> realtimeMarker = new ArrayList<>();


    public GasolinaFragment() {   }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(rootview != null){
            ViewGroup parent = (ViewGroup) rootview.getParent();
            if(parent != null)
                parent.removeView(rootview);
        }try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
            databaseReference = FirebaseDatabase.getInstance().getReference();
//            databaseReference.child("Ubicación").push().setValue(mAuth.getCurrentUser().getEmail());
            rootview = inflater.inflate(R.layout.fragment_gasolina, container, false);
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .enableAutoManage(getActivity(),1, this)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .addApi(LocationServices.API)
                    .build();
            googleApiClient.connect();
            autocompleteFragment = (PlaceAutocompleteFragment)
                    getActivity().getFragmentManager().findFragmentById(place_autocomplete_fragment_gas);
            autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    String placeName = place.getLatLng().toString();
                    String result = placeName.replaceAll("[|?*<\":>+\\[\\]/'latlng():]","").trim();
                    String[] latlon = result.toString().split(",");
                    LatLng brisa = new LatLng(Double.parseDouble(latlon[0]),Double.parseDouble(latlon[1]));
                    Toast.makeText(getActivity(),"" + placeName,Toast.LENGTH_SHORT).show();
                    gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(brisa,7));
                    CameraPosition cam = new CameraPosition.Builder()
                            .target(brisa)
                            .zoom(14)
                            .bearing(0)
                            .tilt(90)
                            .build();
                    gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cam));
                    gMap.addMarker(new MarkerOptions()
                    .position(brisa)
                    .title(""));
                }

                @Override
                public void onError(Status status) {     }
            });

            countDownTimer();

        }catch (InflateException e){   }
        return rootview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapViewG = rootview.findViewById(R.id.mapa_gas); //se cambio de include MapFragment a mapView
        typefilter = new AutocompleteFilter.Builder()
                .setCountry("MEX")
                .build();
        autocompleteFragment.setFilter(typefilter);
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY).build(getActivity());

        }catch (GooglePlayServicesNotAvailableException e){
            e.printStackTrace();
        }catch (GooglePlayServicesRepairableException e){
            e.printStackTrace();
        }
        setRetainInstance(true);
        if(mapViewG != null){
            mapViewG.onCreate(null);
            mapViewG.onResume();
            mapViewG.getMapAsync(this);
        }

    }

    private void countDownTimer(){

        new CountDownTimer(50000,2000) {
            @Override
            public void onTick(long millisUntilFinished) {

                Log.e("Seconds remaining","" + millisUntilFinished / 1000);
            }

            @Override
            public void onFinish() {
                Toast.makeText(getActivity(),"Puntos actualizados",Toast.LENGTH_SHORT).show();
                onMapReady(gMap);
            }
        }.start();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.animateCamera(CameraUpdateFactory.zoomBy(8));
        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        String Gasolina = "gas";
        gMap.clear();



        //Se agregó código para recorrer marcadores

        databaseReference.child("Gasolinerias").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(Marker marker: realtimeMarker){
                    marker.remove();


                }
                //método que obtiene todos los datos que están en Gasolinerias
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){

                    MapPojo mp = snapshot.getValue(MapPojo.class);
                    Double latitud = mp.getLatitud();
                    Double longitud = mp.getLongitud();
                    String nombre = mp.getNombre();
                    String direccion = mp.getDireccion();
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(new LatLng(latitud,longitud)).title(nombre).snippet(direccion)
                            .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.icgas_station));


                    tempRealtimeMarkers.add(gMap.addMarker(markerOptions));
                }
                realtimeMarker.clear();
                realtimeMarker.addAll(tempRealtimeMarkers);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        LatLng brisa = new LatLng(19.4785099, -99.2396317);
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(brisa,8));
        CameraPosition camera = new CameraPosition.Builder()
                .target(brisa)
                .zoom(7)
                .bearing(0)
                .tilt(90)
                .build();
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));

        ubicacion = true;
        marshmallowGPSPermissionCheck();
        gMap.setOnMyLocationClickListener(this);
        gMap.setOnMyLocationButtonClickListener(this);

    }
    //Método para agregar marker gasolinerias
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void marshmallowGPSPermissionCheck(){
        int permission = ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if(permission == PackageManager.PERMISSION_GRANTED){
            gMap.setMyLocationEnabled(true);
        }else {
            ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_LOCATION_REQUEST_CODE);
        }

        //subir Latlong a Firebase
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(Location location) {
                        mAuth = FirebaseAuth.getInstance();

                        if (location != null){

                        // databaseReference.child("Usuario").push().setValue("pruebas@tixmedia.com");

                          /*  Map<String,Object> latlang = new HashMap<>();
                            latlang.put("latitud",location.getLatitude());
                            latlang.put("longitud",location.getLongitude());
                            databaseReference.child("Gasolinerias").push().setValue(latlang);*/
                        }
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode){
            case MY_LOCATION_REQUEST_CODE:{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    //lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,2000,10, locationListener);
                }else{  //permission denied, boo! Disable the
                    //functionality that depends on this permission
                }
                break;
            }
        }
    }

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {       }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {     }

        @Override
        public void onProviderEnabled(String s) {     }

        @Override
        public void onProviderDisabled(String s) {     }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE){
            Place place = PlaceAutocomplete.getPlace(getActivity(),data);
            Log.i(TAG,"Place: " + place.getName());
        }else if(resultCode == PlaceAutocomplete.RESULT_ERROR){
            Status status = PlaceAutocomplete.getStatus(getActivity(), data);
            Log.i(TAG, status.getStatusMessage());
        }else if(resultCode == RESULT_CANCELED){

        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final AutocompletePrediction item = placeAutocompleteAdapter.getItem(position);
        final String placeId = item.getPlaceId();
        Places.GeoDataApi.getPlaceById(googleApiClient,placeId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(@NonNull PlaceBuffer places) {
                        if(places.getStatus().isSuccess() && places.getCount() > 0){
                            final Place myPlace = places.get(0);
                            placeInfo = new PlaceInfo();
                            placeInfo.setName(myPlace.getName().toString());
                            placeInfo.setAddress(myPlace.getAddress().toString());
                            placeInfo.setAttributions(myPlace.getAttributions().toString());
                            placeInfo.setId(myPlace.getId());
                            placeInfo.setLatlng(myPlace.getLatLng());
                            placeInfo.setRating(myPlace.getRating());
                            placeInfo.setPhoneNumber(myPlace.getPhoneNumber().toString());
                            placeInfo.setWebsiteUri(myPlace.getWebsiteUri());

                            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPlace.getLatLng(),7));
                        }else{
                            Log.e(TAG,"Lugar no encontrado");
                        }
                        places.release();
                    }
                });

    }


    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {

    }

    @Override
    public void onPause() {

        super.onPause();
        googleApiClient.stopAutoManage(getActivity());
        googleApiClient.disconnect();
        countDownTimer();
    }

    @Override
    public void onResume() {
        super.onResume();
        countDownTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(googleApiClient != null && googleApiClient.isConnected()){
            googleApiClient.stopAutoManage(getActivity());
            googleApiClient.disconnect();
            countDownTimer();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (autocompleteFragment != null && getActivity() != null && !getActivity().isFinishing()){
            getActivity().getFragmentManager().beginTransaction().remove(autocompleteFragment).commit();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        GasolinaFragment g = (GasolinaFragment) getFragmentManager()
                .findFragmentById(R.id.place_autocomplete_fragment_gas);
        if(g != null)
            getFragmentManager().beginTransaction().remove(g).commit();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
