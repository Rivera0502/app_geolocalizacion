package com.example.fernanda.app_geolocalizacion.Util;

import android.content.SharedPreferences;

public class Utils {

    public static String getUsersMailPref(SharedPreferences preferences){
        return preferences.getString("email","");
    }

    public static String getUsersPasswordPref(SharedPreferences preferences){
        return preferences.getString("pass","");
    }

}
