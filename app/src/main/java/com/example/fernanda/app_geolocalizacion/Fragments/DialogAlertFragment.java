package com.example.fernanda.app_geolocalizacion.Fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fernanda.app_geolocalizacion.Activities.LoginActivity;
import com.example.fernanda.app_geolocalizacion.Activities.MainActivity;
import com.example.fernanda.app_geolocalizacion.R;

public class DialogAlertFragment extends DialogFragment {

    public static final String TITLE = "dialog.alert.TITLE";
    public static final String MESSAGE = "dialog.alert.MESSAGE";
    public static final String BTN_CANCEL = "dialog.alert.BTN_CANCEL";

    private DialogInterface.OnClickListener mOnClickListener;

    public static DialogAlertFragment newInstance(String title, String message, boolean btnCancel) {
        Bundle args = new Bundle();
        if(title != null){
            args.putString(TITLE,title);
        }
        if(message != null){
            args.putString(MESSAGE, message);
        }
        args.putBoolean(BTN_CANCEL,btnCancel);
        DialogAlertFragment dialogAlertFragment = new DialogAlertFragment();
        dialogAlertFragment.setCancelable(false);
        dialogAlertFragment.setArguments(args);
        return dialogAlertFragment;
    }

    public static DialogAlertFragment newInstance(String message, boolean btnCancel) {
        return DialogAlertFragment.newInstance(null,message,btnCancel);
    }

    public static DialogAlertFragment newInstance(String message) {
        return DialogAlertFragment.newInstance(message,false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.DialogTheme);
        builder.setTitle(getArguments().getString(TITLE,getString(R.string.app_name)));
        builder.setMessage(getArguments().getString(MESSAGE));
        builder.setPositiveButton(R.string.alertdialogOK, mOnClickListener);

        if(getArguments().getBoolean(BTN_CANCEL,false)){
            builder.setNegativeButton(R.string.alertdialogCancel,null);
        }

        AlertDialog alertDialog = builder.create();
        return alertDialog;

    }

    public DialogInterface.OnClickListener getOnClickListener() {
        return mOnClickListener;
    }

    public void setOnClickListener(DialogInterface.OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }


}
