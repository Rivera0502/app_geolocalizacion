package com.example.fernanda.app_geolocalizacion.Activities;


import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.fernanda.app_geolocalizacion.Adapters.PageAdapter;
import com.example.fernanda.app_geolocalizacion.Fragments.DialogAlertFragment;
import com.example.fernanda.app_geolocalizacion.Fragments.GasolinaFragment;
import com.example.fernanda.app_geolocalizacion.Fragments.HospitalFragment;
import com.example.fernanda.app_geolocalizacion.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.MapFragment;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth AuthUI;
    private TabLayout tabs;
    private ViewPager viewPager;
    private PageAdapter adapterView;

    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabs = findViewById(R.id.tabLayout);
        initViewPager();

       //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,mapFragment).commit();

        AuthUI = FirebaseAuth.getInstance();
        checkGooglePlayServices();
    }


    private void initViewPager(){
        viewPager = findViewById(R.id.viewPager);
        adapterView = new PageAdapter(getSupportFragmentManager());
        //Add fragmentos
        adapterView.AddFragment(new GasolinaFragment(),"Gasolinerias");
        adapterView.AddFragment(new HospitalFragment(),"Hospitales");
        viewPager.setAdapter(adapterView);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.setupWithViewPager(viewPager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.logOut:
                showMessage(getString(R.string.alertLogOut),(DialogInterface,i) ->{
                    logOut();
                },getString(R.string.app_name),true );
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void logOut(){
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private boolean checkGooglePlayServices(){
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int result = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS){
            if (googleApiAvailability.isUserResolvableError(result)){
                googleApiAvailability.getErrorDialog(this,result,0).show();
            }
            return false;
        }
        return false;
    }


    private void changeFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
           .replace(R.id.viewPager,fragment).commit();
    }

    public void showMessage(String message, DialogInterface.OnClickListener onClickListener, String title, boolean button_cancel){
        DialogAlertFragment alertFragment = DialogAlertFragment.newInstance(title,message,button_cancel);
        alertFragment.setOnClickListener(onClickListener);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(alertFragment, "AlertFragment");
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void showMessage(String message, DialogInterface.OnClickListener onClickListener, String title){
        showMessage(message,onClickListener,title,false);
    }

    protected void showMessage(String message, DialogInterface.OnClickListener onClickListener){
        showMessage(message,onClickListener,null);
    }

    public void showMessage(String message){
        showMessage(message,null);
    }

}