package com.example.fernanda.app_geolocalizacion.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fernanda.app_geolocalizacion.Fragments.DialogAlertFragment;
import com.example.fernanda.app_geolocalizacion.R;
import com.example.fernanda.app_geolocalizacion.Util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG= "EmailPassword";
    private SharedPreferences pref;
    private EditText Useremail, Userpassword;
    private Button btnLogin, btnRegistro;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener authStateListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        UI();
        findViewById(R.id.buttonLogin).setOnClickListener(this);
        findViewById(R.id.buttonSign).setOnClickListener(this);
        pref = getSharedPreferences("Preferences",MODE_PRIVATE);
        setCredentialsIfExist();
        //Obtener la instancia a FirebaseAuthentication
        mAuth = FirebaseAuth.getInstance();

        VerifyConnection();

        Toast.makeText(this, "Si no tiene una cuenta: REGISTRESÉ ",Toast.LENGTH_SHORT).show();

    }

    private void UI(){
        Useremail = findViewById(R.id.editTextEmail);
        Userpassword = findViewById(R.id.editTextPassword);

        btnLogin = findViewById(R.id.buttonLogin);
        btnRegistro = findViewById(R.id.buttonSign);

        Useremail.setHint("Correo electrónico");
        Userpassword.setHint("Contraseña");
    }

    public void setCredentialsIfExist(){
        String email = Utils.getUsersMailPref(pref);
        String password = Utils.getUsersPasswordPref(pref);
        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){
            Useremail.setText(email);
            Userpassword.setText(password);

            Bundle bundle = new Bundle();
            Intent intent = new Intent(this, Map.class);
            Useremail.getText().toString();
            Userpassword.getText().toString();
            bundle.putString("valor_Email",email);
            bundle.putString("valor_Pass",password);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public boolean login (String email, String password){
        if(!isValidEmail(email)){
            showMessage(getString(R.string.emailInvalid));
            return false;
        }else if(!isValidPass(password)){
            showMessage(getString(R.string.passInvalid));
            return false;
        }else{
            return true;
        }
    }

    private void saveOnPreferences(String email,String password){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("email",email);
        editor.putString("pass",password);
        editor.apply();
    }

    public boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isValidPass(String password){
        return password.length()>=6;
    }

    private void goToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if(i == R.id.buttonSign){
            if(login(Useremail.getText().toString(),Userpassword.getText().toString())){
                createAccount(Useremail.getText().toString(),Userpassword.getText().toString());
            }
        }else if(i == R.id.buttonLogin){
            if(login(Useremail.getText().toString(),Userpassword.getText().toString())){
                signIn(Useremail.getText().toString(),Userpassword.getText().toString());
            }
        }
    }

    private void updateUI(FirebaseUser user){
        if(user != null){
            if(user.isEmailVerified()){
                showMessage(getString(R.string.emailNoVerificado));
            }else{
                goToMain();
            }
            }
        }

    //Método que valida si la cuenta ya esta registrada, deja ingresar a la app
    public void createAccount(String email, String password){

        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Log.d(TAG, "createUserWithEmail:success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    updateUI(user);
                }else{
                    Log.d(TAG,"createUserWithEmail:failure",task.getException());
                    showMessage(getString(R.string.cuentaRegistrada));
                    updateUI(null);
                }
            }
        });
    }
    //Método para crear una cuenta nueva (Firebase)
    private void signIn(final String email, final String password){
        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    //Si el registro es exitoso, actualiza UI con la información del usuario
                    Log.d(TAG,"signInWithEmail:success");
                    FirebaseUser user = mAuth.getCurrentUser();
                    updateUI(user);
                }else{
                    //Si el registro falla, muestra un mensaje al usuario
                    Log.w(TAG,"signInWithEmail:failure",task.getException());
                   showMessage(getString(R.string.sinConexion));
                   updateUI(null);
                }
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void VerifyConnection(){
        ConnectivityManager connectivity = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected()){

        }else{
            showMessage(getString(R.string.conexion));
            Intent intentLogin = new Intent(this, LoginActivity.class);
            startActivity(intentLogin);
        }
    }

    public void showMessage(String message, DialogInterface.OnClickListener onClickListener, String title, boolean button_cancel){
        DialogAlertFragment alertFragment = DialogAlertFragment.newInstance(title,message,button_cancel);
        alertFragment.setOnClickListener(onClickListener);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(alertFragment, "AlertFragment");
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void showMessage(String message, DialogInterface.OnClickListener onClickListener, String title){
        showMessage(message,onClickListener,title,false);
    }

    protected void showMessage(String message, DialogInterface.OnClickListener onClickListener){
        showMessage(message,onClickListener,null);
    }

    public void showMessage(String message){
        showMessage(message,null);
    }

}
