package com.example.fernanda.app_geolocalizacion.Fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import static com.example.fernanda.app_geolocalizacion.R.id.place_autocomplete_fragment;
import com.example.fernanda.app_geolocalizacion.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

public class PlacesFragment extends Fragment {

    String TAG = "Selected";
    PlaceAutocompleteFragment autocompleteFragment;

    private View view;

    public PlacesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_places, container, false);
        autocompleteFragment = (PlaceAutocompleteFragment)
                getActivity().getFragmentManager().findFragmentById(place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                Log.i(TAG,"Lugar: " + place.getName());
            }

            @Override
            public void onError(Status status) {
                Log.i(TAG,"Ocurrió un error: " + status);
            }
        });
        return view;
    }

}
