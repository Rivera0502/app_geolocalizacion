package com.example.fernanda.app_geolocalizacion.Splash;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;

import com.example.fernanda.app_geolocalizacion.Activities.LoginActivity;
import com.example.fernanda.app_geolocalizacion.Activities.MainActivity;
import com.example.fernanda.app_geolocalizacion.Activities.MapsActivity;
import com.example.fernanda.app_geolocalizacion.Fragments.MapFragment;
import com.example.fernanda.app_geolocalizacion.R;
import com.example.fernanda.app_geolocalizacion.Util.Utils;

public class SplashScreen extends AppCompatActivity {

    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.splash);

       pref = getSharedPreferences("Preferences",Context.MODE_PRIVATE);

        Intent intentLogin = new Intent(this,LoginActivity.class);
        Intent intentMain = new Intent(this,MainActivity.class);

        if(!TextUtils.isEmpty(Utils.getUsersMailPref(pref)) &&
            !TextUtils.isEmpty(Utils.getUsersPasswordPref(pref))){

            startActivity(intentMain);

        }else{

            startActivity(intentLogin);

        } finish();
    }
}
